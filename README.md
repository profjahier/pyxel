[Rendu du site](https://profjahier.forge.apps.education.fr/pyxel/)


Tutoriels commentés des [exemples de la doc officielle](https://github.com/kitao/pyxel/blob/main/docs/README.fr.md#lancer-les-exemples-de-pyxel) de la librairie pyxel.

