# La librairie pyxel

Ce site a pour but de présenter la librairie pyxel dans l'optique de la préparation à la [Nuit du Code](https://www.nuitducode.net/).

Les tutoriels proposés s'appuient sur les exemples de la [doc officielle](https://github.com/kitao/pyxel/blob/main/docs/README.fr.md#lancer-les-exemples-de-pyxel){target=_blank} de la librairie **pyxel** version 2.


Il est possible de s'entraîner à l'utilisation de cette librairie sans installation en travaillant en ligne sur le site [pyxelstudio](https://www.pyxelstudio.net/){target=_blank}. La création d'un compte (facultative) permet de sauvegarder ses projets.

*Remarque :* Attention, il y a eu quelques changements d'importance entre la version 1 et la version 2, notamment concernant l'accès aux ressources des fichiers pyxres. Les fonctions `tilemap` et `image` sont par exemple remplacées par des variables `tilemaps` et `images` qui sont des listes  (type list). Le format des fichiers pyxres a aussi été modifié. Il semble que les fichiers pyxres des exemples de la documentation officielle ne soient pas compatibles avec la version actuelle de pyxelstudio (cela va sans doute évoluer dans le futur... [*écrit le 01 juin 2024*]).


## Note préliminaire

Tous les tutoriels proposés nécessitent évidemment d'importer la librairie pyxel.
Je propose de le faire sous la forme :
!!! note "import"
    ```python
    import pyxel as px
    ```
Ainsi toutes les fonctions ou constantes de la librairie sont désignées sous la forme `px.fonction` ou `px.CONSTANTE`.

!!! example "Exemple"
    La fonction `run` ou  le code de la touche ++arrow-up++ se nomment :
    ```python
    px.run()
    px.KEY_UP
    ```

## Fonctions de base : `init` et le triplet `run, update, draw` 
Voici la structure minimaliste d'une application utilisant le moteur de jeu pyxel.
!!! abstract ""
    ```python title="jeu.py" linenums="1"
    import pyxel as px

    LARGEUR, HAUTEUR = 256, 128
    px.init(LARGEUR, HAUTEUR, title="Titre du jeu") 

    def update():
        """ Fonction appelée automatiquement et régulièrement. 
        Définit les mises à jour régulières des variables évoluant au cours du jeu. 
        """
        # blablabla

    def draw():
        """ Fonction appelée automatiquement et régulièrement, à la suite de update. 
        Définit les affichages graphiques de tous les élements du jeu.
        """
        # blablabla

    px.run(update, draw) # enclenche la boucle principale qui appelle régulièrement les fonctions update et draw
    ```

La fonction `init` crée la fenêtre de jeu. Les paramètres de base sont la largeur et la hauteur de l'écran de jeu (en pixels) et le titre de l'application.

!!! tip "Constantes"
    En essaiera de s'habituer à définir des constantes, variables conventionnellement écrites en majuscules, qui sont des paramètres du jeu qui ne varient pas au cours du déroulement du jeu.
    En particulier, toutes les dimensions (taille des personnages, des éléments de décors, les tuiles de jeu...) sont définies en début de programme.
    !!! example "Par exemple, ici on a défini et utilisé `LARGEUR`et `HAUTEUR`"


La boucle principale du jeu, qui lance le gestionnaire d'événements à l'écoute des actions du joueurs par la suite (déplacement de souris, appuis sur les touches du clavier, etc), est déclenchée par l'appel de la fonction `run` qui prend en paramètres deux fonctions (`update` et `draw`).

Chacune de ces fonctions est alors appelée automatiquement à intervalles réguliers :

- `update` définit les mises à jour du jeu en fonction des événements qui peuvent survenir
- `draw` actualise l'affichage des éléments graphiques du jeu (personnages, décors...)

*Remarque :* les noms exacts de ces 2 fonctions sont libres, mais l'usage (même en Français) est de garder les noms originaux *update* et *draw*. La fonction `run` en revanche fait partie de la librairie et son nom est donc obligatoire ! (ainsi que la fonction `init`).

!!! tip "`framecount` et `fps`"
    - `fps` (**f**rame **p**ar **s**econde) est un paramètre qui peut-être définit optionnellement dans la fonction `init` pour indiquer à quel rythme rafraîchir l'affichage du jeu, c'est à dire le nombre d'images par seconde du jeu. On utilisera d'ailleurs  plutôt le terme anglais **frame** pour désigner ces images. Cela définit donc à quel rythme les fonctions `update` et `draw` sont appelées.
    - `px.framecount` est un compteur qui contient le numéro de la *frame* en cours. Ce compteur est actualisé automatiquement au rythme de *fps*.
