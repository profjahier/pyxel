---
author: profjahier
title: Tuto Jump game
---

# Jump game

## La version officielle (02_jump_game.py)
[Tester online :fontawesome-solid-paper-plane:](https://kitao.github.io/pyxel/wasm/examples/02_jump_game.html){.md-button target=_blank}

!!! abstract "[Voir le code originel](https://github.com/kitao/pyxel/blob/main/python/pyxel/examples/02_jump_game.py){target=_blank}  sur Github"
 
 
##  Quelques bonnes pratiques de programmation
Je ne vais pas détailler tout le code, mais simplement identifier les patrons de programmation et les méthodes intéressantes mises en oeuvre qui pourraient resservir dans d'autres jeux.

Un "patron de programmation" est un modèle d'organisation du code que l'on peut retrouver dans de nombreux programmes et qui permet d'avoir une structure de code bien élaborée et reproductible.

### "Découper" les fonctions `update` et `draw` 
!!! tip "Une fonction `update` et `draw` par élément de jeu"
    Le premier patron de programmation à retenir est la séparation de la fonction `update` générale du jeu en différentes fonctions `update_element` définissant une fonction de mise à jour pour chaque élément du jeu. 

    Le même principe s'applique bien entendu à la fonction `draw`.

Le code de ces fonctions  suivant ce patron ressemble donc à ceci :
!!! abstract ""
    ```python  
    def update():
        update_decors()
        update_heros()
        for e in ennemis:     # ennemis est une liste d'adversaires 
            update_ennemi(e)  # et l'on met à jour chaque adversaire

    def draw():
        draw_decors()
        draw_heros()
        for e in ennemis:     
            draw_ennemi(e)     
    ```

### Exploitation d'une variable `is_alive`
À chaque élément du jeu, comme le personnage ou les fruits ou les plateformes dans cet exemple, est associé un booléen permettant de savoir si l'élément est toujours actif dans le jeu ou non.

Les fonctions `update` et `draw` de chaque élément ont un comportement différent si l'élément est actif (`is_alive` vaut `#!py True`) ou non. Typiquement, lorsque l'élément n'est plus actif, aucune mise à jour ni affichage à l'écran n'est nécessaire.

!!! example "Exemple : Suivi d'une variable globale `heros_isalive`" 

    ```python  
    def update_heros():
        if heros_isalive:
            # maj des coordonnées du joueur,
            # etc
        else:
            # mettons fin au jeu !
    ```
    *Remarque :* En POO, `isalive` sera certainement un attribut de la classe modélisant une instance de `heros` (on écrira alors `heros.isalive`)

Éventuellement la variable référençant un élément de jeu devenu inactif peut être supprimée (pour libérer un peu de mémoire) avec une instruction `#!py del element`.

Signalons par ailleurs que le suivi d'une variable globale pour identifier une phase du jeu est également une pratique usuelle pour adapter le comporterment des fonctions `update` ou `draw` en fonction de l'état du jeu (par exemple en pause, ou jeu au niveau *n*, ou sur un écran d'accueil, ou de score final, etc).

!!! example "Exemple : Suivi d'une variable globale `scene`" 

    ```python  
    def update():
        if scene == "accueil":
            # code du jeu en mode écran d'accueil
        elif scene == "niveau standard":
            # code du jeu standard
        elif scene == "niveau boss":
            # code du jeu pour un niveau spécial
        elif scene == "hall of fame":
            # code du jeu pour afficher les meilleurs scores
    ```

## Code source brut selon la doc officielle
On pourra lire ici le code complet sans commentaire. La section suivante présente des commentaires assez généraux permettant de mieux comprendre les étapes de ce code. Je conseille de lire les deux en parallèle.
???- abstract "Code source"
    ```python title="jeu.py" linenums="1"
    import pyxel as px

    class App:
        def __init__(self):
            px.init(160, 120, title="Pyxel Jump")
            px.load("assets/jump_game.pyxres")
            self.score = 0
            self.player_x = 72
            self.player_y = -16
            self.player_dy = 0
            self.is_alive = True
            self.far_cloud = [(-10, 75), (40, 65), (90, 60)]
            self.near_cloud = [(10, 25), (70, 35), (120, 15)]
            self.floor = [(i * 60, px.rndi(8, 104), True) for i in range(4)]
            self.fruit = [
                (i * 60, px.rndi(0, 104), px.rndi(0, 2), True) for i in range(4)
            ]
            px.playm(0, loop=True)
            px.run(self.update, self.draw)

        def update(self):
            if px.btnp(px.KEY_Q):
                px.quit()

            self.update_player()
            for i, v in enumerate(self.floor):
                self.floor[i] = self.update_floor(*v)
            for i, v in enumerate(self.fruit):
                self.fruit[i] = self.update_fruit(*v)

        def update_player(self):
            if px.btn(px.KEY_LEFT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_LEFT):
                self.player_x = max(self.player_x - 2, 0)
            if px.btn(px.KEY_RIGHT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_RIGHT):
                self.player_x = min(self.player_x + 2, px.width - 16)
            self.player_y += self.player_dy
            self.player_dy = min(self.player_dy + 1, 8)

            if self.player_y > px.height:
                if self.is_alive:
                    self.is_alive = False
                    px.play(3, 5)
                if self.player_y > 600:
                    self.score = 0
                    self.player_x = 72
                    self.player_y = -16
                    self.player_dy = 0
                    self.is_alive = True

        def update_floor(self, x, y, is_alive):
            if is_alive:
                if (
                    self.player_x + 16 >= x
                    and self.player_x <= x + 40
                    and self.player_y + 16 >= y
                    and self.player_y <= y + 8
                    and self.player_dy > 0
                ):
                    is_alive = False
                    self.score += 10
                    self.player_dy = -12
                    px.play(3, 3)
            else:
                y += 6
            x -= 4
            if x < -40:
                x += 240
                y = px.rndi(8, 104)
                is_alive = True
            return x, y, is_alive

        def update_fruit(self, x, y, kind, is_alive):
            if is_alive and abs(x - self.player_x) < 12 and abs(y - self.player_y) < 12:
                is_alive = False
                self.score += (kind + 1) * 100
                self.player_dy = min(self.player_dy, -8)
                px.play(3, 4)
            x -= 2
            if x < -40:
                x += 240
                y = px.rndi(0, 104)
                kind = px.rndi(0, 2)
                is_alive = True
            return (x, y, kind, is_alive)

        def draw(self):
            px.cls(12)

            # Draw sky
            px.blt(0, 88, 0, 0, 88, 160, 32)

            # Draw mountain
            px.blt(0, 88, 0, 0, 64, 160, 24, 12)

            # Draw trees
            offset = px.frame_count % 160
            for i in range(2):
                px.blt(i * 160 - offset, 104, 0, 0, 48, 160, 16, 12)

            # Draw clouds
            offset = (px.frame_count // 16) % 160
            for i in range(2):
                for x, y in self.far_cloud:
                    px.blt(x + i * 160 - offset, y, 0, 64, 32, 32, 8, 12)
            offset = (px.frame_count // 8) % 160
            for i in range(2):
                for x, y in self.near_cloud:
                    px.blt(x + i * 160 - offset, y, 0, 0, 32, 56, 8, 12)

            # Draw floors
            for x, y, is_alive in self.floor:
                px.blt(x, y, 0, 0, 16, 40, 8, 12)

            # Draw fruits
            for x, y, kind, is_alive in self.fruit:
                if is_alive:
                    px.blt(x, y, 0, 32 + kind * 16, 0, 16, 16, 12)

            # Draw player
            px.blt(
                self.player_x,
                self.player_y,
                0,
                16 if self.player_dy > 0 else 0,
                0,
                16,
                16,
                12,
            )

            # Draw score
            s = f"SCORE {self.score:>4}"
            px.text(5, 4, s, 1)
            px.text(4, 4, s, 7)

    App()
    ```

## Code complet pseudo code / commentaires
Ci-dessous, je propose de parcourir l'ensemble du code en indiquant simplement quelques commentaires pour comprendre la logique du code sans entrer dans des détails très spécifiques. 
???+ abstract "Code commenté"
    ```python title="jeu.py"  
    class App:
        def __init__(self):
            #init : initialisation du jeu
            #load : chargement d'une ressource pyxres
            #score : variable générale de score
            #player_x, player_y, player_dy, is_alive : init des paramètres du joueur
            #far_cloud, near_cloud : liste des coordonnées où dessiner les tuiles nuages
            #floor : liste des plateformes (x, y, bool_is_alive)
            #fruit : liste des fruits (x, y, type_de_fruit, bool_is_alive)
            #playm : joue de la musique
            #run : lancement de la boucle de jeu

        def update(self):
            # maj du joueur
            # maj des plateformes (parcours de la liste floor)
            # maj des fruits (parcours de la liste fruits)

        def update_player(self):
            # maj des coordonnées du joueurs de façon partiellement automatique ou en fonction des touches détectées
            # maj de is_alive en fonction de la position atteinte du joueur

        def update_floor(self, x, y, is_alive):
            if is_alive:
                if (le joueur touche une plateforme):
                    # is_alive = False : élimination de la plateforme
                    # self.score += 10 : maj du score
                    # self.player_dy = -12 : saut du joueur automatique  
                    # px.play(3, 3) : on joue un son spécificique 
            else:
                # y += 6 : déplacement vertical de la plateforme
            # actualisation des données de la plateforme

        def update_fruit(self, x, y, kind, is_alive):
            if is_alive and (le joueur touche le fruit):
                # is_alive = False : élimination du fruit
                # self.score += (kind + 1) * 100 : maj du score
                # self.player_dy = min(self.player_dy, -8) : chute du joueur automatique  
                # px.play(3, 4) : on joue un son spécificique 
            # actualisation des données du fruit 

        def draw(self):
            """ Rappel des paramètres de la fonction blt pour dessiner une portion de la banque d'images
            sur l'écran de jeu :
            px.blt(x_ecran, y_ecran, banque_img_n°, x_img, y_img, largeur_img, hauteur_img)
            """
            # cls(12) : colorie le fond d'écran

            # Draw sky, mountain : no comment 

            # Draw trees, clouds :
            # offset = (frame_count // p) % n : cf 'Astuces pour les affichages'
            # 32 + kind * 16  : cf 'Astuces pour les affichages'

            # Draw floors, fruits : seuls les éléments actifs sont dessinés (is_alive vaut True)

            # Draw player
            # 16 if self.player_dy > 0 else 0 : cf 'Astuces pour les affichages'

            # Draw score : affichage textuel du score 

    App()
    ```

## Astuces pour les affichages `(px.frame_count // p) % n`
Pour modifier les positions d'affichage d'un élément ou modifier son aspect au cours du temps, on peut exploiter le paramètre `frame_count` qui, rappelons-le, contient le numéro de la *frame* en cours (incrémenté au rythme de *fps* valant 30 par défaut).

Voici donc la formule de conversion entre le `frame_count` et le temps `t` mesuré en secondes :

 `t = frame_count / fps `

En effectuant une division entière, remarquons alors que `frame_count // fps` garde la même valeur pendant toute une seconde.

Si on veut enfin un affichage **périodique** d'une succession de `n` images, le modulo vient à notre secours. En effet, `(frame_count // fps) % n` prend successivement toutes les valeurs entières de `0` à `n-1` à chaque seconde.

Si les images sont enregistrées dans une liste `images`, alors `images[(frame_count // fps) % n]` désigne bien successivement et périodiquement chacune des images de la série d'images.

Cependant, dans une application pyxel, les images à afficher sur l'écran de jeu ne sont pas enregistrées dans une simple liste, mais sont chargées depuis le fichier de ressources pyxres. La dernière astuce vient donc de la construction pertinente de la banque d'images. Il suffit simplement que la série d'images qui nous intéressent soient dessinées les unes à la suite des autres dans la banque d'images.

Considérons un exemple concret de 3 images de 8 pixels de large (et h px de haut) dessinées en ligne aux coordonnées (0, y), (8, y) et (16, y) de la banque d'images n°0.

L'instruction `#!py px.blt(x_ecran, y_ecran, 0, 8 * (px.frame_count // fps) % 3), y, 8, h)` affiche donc successivement et périodiquement ces 3 images à l'écran.

Retenez bien cette astuce qui est par exemple très fréquente pour donner un aspect d'animation d'un personnage à l'écran au cours de son déplacement (on change périodiquement l'image du personnage).

Encore un dernier détail, si on ne veut pas une modification d'aspect toutes les secondes, mais à une autre fréquence, il suffit simplement de diviser `frame_count` par une autre valeur que `fps` (en tâtonnant un peu, on trouve vite la valeur qui rend l'animation fluide) !

