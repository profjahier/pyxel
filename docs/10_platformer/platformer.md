---
author: profjahier
title: Tuto Platformer
---

# Platformer

## La version officielle (10_platformer.py)
[Tester online :fontawesome-solid-paper-plane:](https://kitao.github.io/pyxel/wasm/examples/10_platformer.html){.md-button target=_blank}

!!! abstract "[Voir le code originel](https://github.com/kitao/pyxel/blob/main/python/pyxel/examples/10_platformer.py){target=_blank}  sur Github"
 
## Deux nouveaux patrons de programmation

### Préliminaire
Il  est indispensable de maîtriser le [chapitre traitant des fichiers pyxres et des coordonnées](../../Pyxres/01-pyxres)


### Patron 1 : détection des collisions (fonction `is_colliding`)
Dans ce jeu *Platformer*, comme dans beaucoup d'autres jeux, il est nécessaire de pouvoir détecter si un sprite entre en collision avec le décor. Il faut donc savoir si un des pixels du sprite (de largeur L et de hauteur H) se situe éventuellement sur une tuile décor. La méthode `pget` d'une tilemap (cf [tuto pget](../../Pyxres/02-interaction_decor/#identifier-une-tuile-methode-pget)) permet de connaître le type de tuile (défini par un couple (x_tile_banq, y_tile_banq) donnant ses coordonnées dans une banque d'images) d'une tuile dessinée aux coordonnées (x, y) de l'écran.

Il faut commencer par obtenir les coordonnées "de tuiles" (x_tile, y_tile) de la tilemap couvertes par les 4 coins du sprite (en bordure du sprite), dont la position est connue à partir des coordonnnées (en pixels) de son coin supérieur gauche (x, y). 

Une tuile mesure 8 pixels de large, et donc les tuiles couvertes par le bord *gauche* du sprite ont pour abscisse : `#!py x_tuile = x1 = x // 8`.

Les tuiles couvertes par le bord *droit* du sprite (de largeur L) ont pour abscisse : `#!py x_tuile = x2 = (x + L - 1) // 8`.

De façon similaire, pour les ordonnées des tuiles couvertes par le bord *haut* ou le bord *bas* du sprite (de hauteur H), on obtient : `#!py y_tuile = y1 = y // 8` et `#!py y_tuile = y2 = (y + H - 1) // 8`.

Il y a collision entre le sprite et le décor si une des tuiles couvertes par le sprite appartient au décor. On parcourt donc l'ensemble des tuiles avec `x1 < x_tile < x2` et `y1 < y_tile < y2` à la recherche d'une tuile de type décor.

Cela nécessite évidemment de connaître les coordonnées de toutes les tuiles décor dans la banque d'images. Ainsi, on notera en général ces coordonnées dans des constantes en début de programme. (ex : `TUILES_DECOR = [(2, 3), (2, 4), (3, 3)]`).

La détection de collision prend alors le modèle suivant :
```python
def is_colliding(x, y):
    x1, x2 = int(x // 8), int((x + 7) // 8)   # sprite de 8 pixels de largeur
    y1, y2 = int(y // 8), int((y + 7) // 8)   # et de 8 pixels de hauteur
    for y_tuile in range(y1, y2 + 1):
        for x_tuile in range(x1, x2 + 1):
            if px.tilemaps[0].pget(x_tuile, y_tuile) in TUILES_DECOR:
                return True # collision détectée
    return False # parcourt complet => aucune tuile couverte par le sprite
```

### Patron 2 : "repositionnement" du joueur lors d'un déplacement (fonction `push_back`)
Lorsqu'une action de jeu commande un déplacement d'un sprite de `dx` pixels horizontalement et de `dy` pixels verticalement (par exemple lorsque le joueur appuie sur une touche de saut ou une touche directionnelle), il faut vérifier si ce déplacement peut être effectué dans sa totalité ou bien si un élément de décor gène ce déplacement.

L'idée principale consiste à déplacer de façon élémentaire (un pixel par un pixel, "step by step") le sprite tant qu'il ne rentre pas en contact avec le décor.

Par ailleurs, le déplacement vertical et le déplacement horizontal doivent s'effectuer l'un après l'autre. On commencera toujours par le déplacement le plus important (ex : si *dx = 5* et *dy = -12*, on commence par le déplacement vertical).

La figure suivante illustre des déplacements et permet de comprendre pourquoi on choisit d'abord le plus grand déplacement horizontal ou vertical (le carré rouge représente le sprite, le carré orange la position visée selon le déplacement souhaité, et les barres noires sont les obstacles) :

![déplacements autorisés](deplacement.png)

Expliquons maintenant en détail l'extrait de code suivant de la fonction `push_back` du jeu, décrivant un déplacement horizontal : 

=== "code généraliste"
    ```python linenums="1"
    for i in range(abs(dx)):
        step = max(-1, min(1, dx))
        if is_colliding(x + step, y):
            break
        x = x + step
        dx = dx - step
    ```         

=== "code spécifique du jeu"
    ```python linenums="1"
    for _ in range(px.ceil(abs_dx)):
        step = max(-1, min(1, dx))
        if is_colliding(x + step, y, dy > 0):
            break
        x += step
        dx -= step
    ``` 
    Ce code prévoit 2 spécificités liées à ce jeu : un déplacement dx non entier, et la gestion du booléen `dy > 0` pour identifier un joueur en cours de chute.

- ligne 1 : on repète `abs(dx)` fois l'action d'avancer de 1 pas (1 step = 1 pixel), mais n'oublions pas que dx peut être négatif.
- ligne 2 : belle astuce pour avoir un `step` qui vaut toujours +1 (si le déplacement est vers la droite, dx > 0) ou -1 (si le déplacement est vers la gauche, dx < 0). 

*Exercice :* Chercher la valeur de `step` si `dx = -5`, `dx = -1`, `dx = 0`, `dx = 1` ou `dx = 5` !
???- tip "Solution"
    - `dx = -5` => `step = -1`
    - `dx = -1` => `step = -1`
    - `dx =  0` => `step = 0`
    - `dx =  1` => `step = 1`
    - `dx =  5` => `step = 1`
    
- lignes 3-4: on teste si une collision avec le décor survient en positionnnant le sprite d'un pas sur le côté (x + step) par rapport à sa position actuelle x. Le cas échéant, on stoppe le déplacement du sprite en conservant sa position x actuelle : `break` est une instruction qui permet de quitter la boucle `for` prématurément.
- lignes 5-6 : s'il n'y a pas eu de collision, la position du sprite est mise à jour "en avançant d'un pas" (rappel 1 step = 1 pixel) ; et l'avancement restant dx est aussi mis à jour en le diminuant d'un pas.

La fonction `push_back` est appelée avec les valeurs actuelles de x et y, ainsi que les déplacements dx et dy souhaités ; et elle renvoie les valeurs prises par x et y à la fin du déplacement possible en fonction du décor.

Son utilisation dans une fonction `update` ressemble donc à ceci :
```python
dx, dy = # dépend des commandes du joueurs
x, y = push_back(x, y, dx, dy)
```
Cela a bien pour effet  de mettre à jour les coordonnées (x, y) pour le déplacement souhaité.

Pour terminer, voici une version en pseudo-code de l'intégralité de la fonction de "repositionnement" (push_back) :
```
Fonction repositionnement(x, y, dx, dy): 
    Si dx > dy: # deplacement horizontal plus important
        Pour i de 1 à abs(dx):
            pas = 1 si dx > 0 ou -1 si dx < 0
            Si collision à (x + pas):
                Quitter la boucle
            Incrémenter x de pas
            Décrémenter dx de pas
        Pour i de 1 à abs(dy):
            pas = 1 si dy > 0 ou -1 si dy < 0
            Si collision à (y + step):
                Quitter la boucle
            Incrémenter y de pas
            Décrémenter dy de pas
    Sinon: # deplacement vertical plus important
        Pour i de 1 à abs(dy):
            pas = 1 si dy > 0 ou -1 si dy < 0
            Si collision à (y + pas):
                Quitter la boucle
            Incrémenter y de pas
            Décrémenter dy de pas
        Pour i de 1 à abs(dx):
            pas = 1 si dx > 0 ou -1 si dx < 0
            Si collision à (x + pas):
                Quitter la boucle
            Incrémenter x de pas
            Décrémenter dx de pas
    Renvoyer x, y
``` 
  
## Code source brut selon la doc officielle
On pourra lire ici le code complet sans commentaire. La section suivante présente des commentaires assez généraux permettant de mieux comprendre les étapes de ce code. 
???- abstract "Montrer/Cacher le code source complet"
    ```python title="jeu.py" linenums="1"
    import pyxel as px 

    TRANSPARENT_COLOR = 2
    SCROLL_BORDER_X = 80
    TILE_FLOOR = (1, 0)
    TILE_SPAWN1 = (0, 1)
    TILE_SPAWN2 = (1, 1)
    TILE_SPAWN3 = (2, 1)
    WALL_TILE_X = 4

    scroll_x = 0
    player = None
    enemies = []

    def get_tile(tile_x, tile_y):
        return px.tilemaps[0].pget(tile_x, tile_y)

    def is_colliding(x, y, is_falling):
        x1 = int(x // 8)
        y1 = int(y // 8)
        x2 = int((x + 7) // 8)
        y2 = int((y + 7) // 8)
        for yi in range(y1, y2 + 1):
            for xi in range(x1, x2 + 1):
                if get_tile(xi, yi)[0] >= WALL_TILE_X:
                    return True
        if is_falling and y % 8 == 1:
            for xi in range(x1, x2 + 1):
                if get_tile(xi, y1 + 1) == TILE_FLOOR:
                    return True
        return False

    def push_back(x, y, dx, dy):
        abs_dx = abs(dx)
        abs_dy = abs(dy)
        if abs_dx > abs_dy:
            for _ in range(px.ceil(abs_dx)):
                step = max(-1, min(1, dx))
                if is_colliding(x + step, y, dy > 0):
                    break
                x += step
                dx -= step
            for _ in range(px.ceil(abs_dy)):
                step = max(-1, min(1, dy))
                if is_colliding(x, y + step, dy > 0):
                    break
                y += step
                dy -= step
        else:
            for _ in range(px.ceil(abs_dy)):
                step = max(-1, min(1, dy))
                if is_colliding(x, y + step, dy > 0):
                    break
                y += step
                dy -= step
            for _ in range(px.ceil(abs_dx)):
                step = max(-1, min(1, dx))
                if is_colliding(x + step, y, dy > 0):
                    break
                x += step
                dx -= step
        return x, y

    def is_wall(x, y):
        tile = get_tile(x // 8, y // 8)
        return tile == TILE_FLOOR or tile[0] >= WALL_TILE_X

    def spawn_enemy(left_x, right_x):
        left_x = px.ceil(left_x / 8)
        right_x = px.floor(right_x / 8)
        for x in range(left_x, right_x + 1):
            for y in range(16):
                tile = get_tile(x, y)
                if tile == TILE_SPAWN1:
                    enemies.append(Enemy1(x * 8, y * 8))
                elif tile == TILE_SPAWN2:
                    enemies.append(Enemy2(x * 8, y * 8))
                elif tile == TILE_SPAWN3:
                    enemies.append(Enemy3(x * 8, y * 8))

    def cleanup_entities(entities):
        for i in range(len(entities) - 1, -1, -1):
            if not entities[i].is_alive:
                del entities[i]

    class Player:
        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.dx = 0
            self.dy = 0
            self.direction = 1
            self.is_falling = False

        def update(self):
            global scroll_x
            last_y = self.y
            if px.btn(px.KEY_LEFT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_LEFT):
                self.dx = -2
                self.direction = -1
            if px.btn(px.KEY_RIGHT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_RIGHT):
                self.dx = 2
                self.direction = 1
            self.dy = min(self.dy + 1, 3)
            if px.btnp(px.KEY_SPACE) or px.btnp(px.GAMEPAD1_BUTTON_A):
                self.dy = -6
                px.play(3, 8)
            self.x, self.y = push_back(self.x, self.y, self.dx, self.dy)
            if self.x < scroll_x:
                self.x = scroll_x
            if self.y < 0:
                self.y = 0
            self.dx = int(self.dx * 0.8)
            self.is_falling = self.y > last_y

            if self.x > scroll_x + SCROLL_BORDER_X:
                last_scroll_x = scroll_x
                scroll_x = min(self.x - SCROLL_BORDER_X, 240 * 8)
                spawn_enemy(last_scroll_x + 128, scroll_x + 127)
            if self.y >= px.height:
                game_over()

        def draw(self):
            u = (2 if self.is_falling else px.frame_count // 3 % 2) * 8
            w = 8 if self.direction > 0 else -8
            px.blt(self.x, self.y, 0, u, 16, w, 8, TRANSPARENT_COLOR)

    class Enemy1:
        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.dx = 0
            self.dy = 0
            self.direction = -1
            self.is_alive = True

        def update(self):
            self.dx = self.direction
            self.dy = min(self.dy + 1, 3)
            if self.direction < 0 and is_wall(self.x - 1, self.y + 4):
                self.direction = 1
            elif self.direction > 0 and is_wall(self.x + 8, self.y + 4):
                self.direction = -1
            self.x, self.y = push_back(self.x, self.y, self.dx, self.dy)

        def draw(self):
            u = px.frame_count // 4 % 2 * 8
            w = 8 if self.direction > 0 else -8
            px.blt(self.x, self.y, 0, u, 24, w, 8, TRANSPARENT_COLOR)

    class Enemy2:
        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.dx = 0
            self.dy = 0
            self.direction = 1
            self.is_alive = True

        def update(self):
            self.dx = self.direction
            self.dy = min(self.dy + 1, 3)
            if is_wall(self.x, self.y + 8) or is_wall(self.x + 7, self.y + 8):
                if self.direction < 0 and (
                    is_wall(self.x - 1, self.y + 4) or not is_wall(self.x - 1, self.y + 8)
                ):
                    self.direction = 1
                elif self.direction > 0 and (
                    is_wall(self.x + 8, self.y + 4) or not is_wall(self.x + 7, self.y + 8)
                ):
                    self.direction = -1
            self.x, self.y = push_back(self.x, self.y, self.dx, self.dy)

        def draw(self):
            u = px.frame_count // 4 % 2 * 8 + 16
            w = 8 if self.direction > 0 else -8
            px.blt(self.x, self.y, 0, u, 24, w, 8, TRANSPARENT_COLOR)

    class Enemy3:
        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.time_to_fire = 0
            self.is_alive = True

        def update(self):
            self.time_to_fire -= 1
            if self.time_to_fire <= 0:
                dx = player.x - self.x
                dy = player.y - self.y
                sq_dist = dx * dx + dy * dy
                if sq_dist < 60**2:
                    dist = px.sqrt(sq_dist)
                    enemies.append(Enemy3Bullet(self.x, self.y, dx / dist, dy / dist))
                    self.time_to_fire = 60

        def draw(self):
            u = px.frame_count // 8 % 2 * 8
            px.blt(self.x, self.y, 0, u, 32, 8, 8, TRANSPARENT_COLOR)

    class Enemy3Bullet:
        def __init__(self, x, y, dx, dy):
            self.x = x
            self.y = y
            self.dx = dx
            self.dy = dy
            self.is_alive = True

        def update(self):
            self.x += self.dx
            self.y += self.dy

        def draw(self):
            u = px.frame_count // 2 % 2 * 8 + 16
            px.blt(self.x, self.y, 0, u, 32, 8, 8, TRANSPARENT_COLOR)

    class App:
        def __init__(self):
            px.init(128, 128, title="Pyxel Platformer")
            px.load("assets/platformer.pyxres")

            # Change enemy spawn tiles invisible
            px.images[0].rect(0, 8, 24, 8, TRANSPARENT_COLOR)

            global player
            player = Player(0, 0)
            spawn_enemy(0, 127)
            px.playm(0, loop=True)
            px.run(self.update, self.draw)

        def update(self):
            if px.btn(px.KEY_Q):
                px.quit()

            player.update()
            for enemy in enemies:
                if abs(player.x - enemy.x) < 6 and abs(player.y - enemy.y) < 6:
                    game_over()
                    return
                enemy.update()
                if enemy.x < scroll_x - 8 or enemy.x > scroll_x + 160 or enemy.y > 160:
                    enemy.is_alive = False
            cleanup_entities(enemies)

        def draw(self):
            px.cls(0)

            # Draw level
            px.camera()
            px.bltm(0, 0, 0, (scroll_x // 4) % 128, 128, 128, 128)
            px.bltm(0, 0, 0, scroll_x, 0, 128, 128, TRANSPARENT_COLOR)

            # Draw characters
            px.camera(scroll_x, 0)
            player.draw()
            for enemy in enemies:
                enemy.draw()

    def game_over():
        global scroll_x, enemies
        scroll_x = 0
        player.x = 0
        player.y = 0
        player.dx = 0
        player.dy = 0
        enemies = []
        spawn_enemy(0, 127)
        px.play(3, 9)

    App()
    ```

## Quelques commentaires sur le code
Ci-dessous, je propose de parcourir l'ensemble du code en donnant quelques commentaires :

!!! note "Lignes 3 à 9 : paramétres du jeu"
    En début de programme, on définit plusieurs constantes en lien avec des paramètres de jeu. 
    
    En particulier ici, on définit les coordonnées de tuiles du décor dans la banque d'images (x_tuile_banq, y_tuile_banq). Le paramètre `WALL_TILE_X = 4` permet de repérer tous les murs qui dans la banque d'images sont dessinées avec x_tuile_banq >= 4.
    
    ???- abstract "Montrer/Cacher le code"
        ```python linenums="3" 
        TRANSPARENT_COLOR = 2
        SCROLL_BORDER_X = 80
        TILE_FLOOR = (1, 0)
        TILE_SPAWN1 = (0, 1)
        TILE_SPAWN2 = (1, 1)
        TILE_SPAWN3 = (2, 1)
        WALL_TILE_X = 4
        ```

!!! note "Lignes 11 à 13 : variables globales"
    Voir directement les commentaires dans le code
    
    ???- abstract "Montrer/Cacher le code"
        ```python linenums="11" 
        scroll_x = 0    # permet de repérer l'avancée horizontale du joueur 
        player = None   # défini en variable globale, player pourra être vu dans toutes les autres classes
        enemies = []    # liste des ennemis mise à jour au cours du jeu
        ```

!!! note "Lignes 15 à 66 : Collisions avec le décor, 'repositionnement'"
    Le code se comprend aisément si on a bien lu les patrons de programmation sur les [collisions](#patron-1-detection-des-collisions-fonction-is_colliding), et celui sur le [repositionnement](#patron-2-repositionnement-du-joueur-lors-dun-deplacement-fonction-push_back).

    Ce jeu gère par ailleurs un booléen `is_falling` identifiant si le héros est en train de chuter ou non (ce booléen est mis à jour en comparant la position verticale `y` de la frame en cours à la position verticale `y_old` de la frame précédente).
    
    ???- abstract "Montrer/Cacher le code"
        ```python linenums="15" 
        def get_tile(tile_x, tile_y):
            return px.tilemaps[0].pget(tile_x, tile_y)

        def is_colliding(x, y, is_falling):
            x1 = int(x // 8)
            y1 = int(y // 8)
            x2 = int((x + 7) // 8)
            y2 = int((y + 7) // 8)
            for yi in range(y1, y2 + 1):
                for xi in range(x1, x2 + 1):
                    if get_tile(xi, yi)[0] >= WALL_TILE_X:
                        return True
            if is_falling and y % 8 == 1:
                for xi in range(x1, x2 + 1):
                    if get_tile(xi, y1 + 1) == TILE_FLOOR:
                        return True
            return False

        def push_back(x, y, dx, dy):
            abs_dx = abs(dx)
            abs_dy = abs(dy)
            if abs_dx > abs_dy:
                for _ in range(px.ceil(abs_dx)):
                    step = max(-1, min(1, dx))
                    if is_colliding(x + step, y, dy > 0):
                        break
                    x += step
                    dx -= step
                for _ in range(px.ceil(abs_dy)):
                    step = max(-1, min(1, dy))
                    if is_colliding(x, y + step, dy > 0):
                        break
                    y += step
                    dy -= step
            else:
                for _ in range(px.ceil(abs_dy)):
                    step = max(-1, min(1, dy))
                    if is_colliding(x, y + step, dy > 0):
                        break
                    y += step
                    dy -= step
                for _ in range(px.ceil(abs_dx)):
                    step = max(-1, min(1, dx))
                    if is_colliding(x + step, y, dy > 0):
                        break
                    x += step
                    dx -= step
            return x, y

        def is_wall(x, y):
            tile = get_tile(x // 8, y // 8)
            return tile == TILE_FLOOR or tile[0] >= WALL_TILE_X
        ```

!!! note "Lignes 68 à 84 : Apparition des ennemis et suppression des entités 'mortes'"
    La fonction `spawn_enemy` parcourt l'espace accessible au joueur à la recherche de tuiles désignant un point d'apparition d'un ennemi (tuile du type `TILE_SPAWN_x`), et crée un nouvel ennemi à cet emplacement en instanciant la classe  `Ennemy` correspondante. Chaque nouvel ennemi est ajouté à la liste globale `ennemies`.

    La fonction `cleanup_entities` purge la liste globale `ennemies` en supprimant les entités dont l'atribut `is_alive` vaut `False`.

    ???- abstract "Montrer/Cacher le code"
        ```python linenums="68" 
        def spawn_enemy(left_x, right_x):
            left_x = px.ceil(left_x / 8)
            right_x = px.floor(right_x / 8)
            for x in range(left_x, right_x + 1):
                for y in range(16):
                    tile = get_tile(x, y)
                    if tile == TILE_SPAWN1:
                        enemies.append(Enemy1(x * 8, y * 8))
                    elif tile == TILE_SPAWN2:
                        enemies.append(Enemy2(x * 8, y * 8))
                    elif tile == TILE_SPAWN3:
                        enemies.append(Enemy3(x * 8, y * 8))

        def cleanup_entities(entities):
            for i in range(len(entities) - 1, -1, -1):
                if not entities[i].is_alive:
                    del entities[i]
        ```

!!! note "Lignes 86 à 126 : Classe Player"
    
    La variable globale `player` est une instance de la classe `Player` créée une unique fois dans l'initialisation de la classe principale `App`.

    Le joueur tombe de façon automatique et se déplace vers le haut ou horizontalement en fonction de certaines touches du clavier.

    Chaque touche clavier définit un déplacement `dx` et `dy` **a priori**. La position réellement atteinte par le joueur est obtenue par l'appel de la fonction `push_back` (rappelons que cette fonction déplace le joueur pixel par pixel jusqu'à rencontrer un obstacle, dans la limite d'un déplacement de dx pixels horizontalement et dy pixels verticalement).

    La position du joueur a pour conséquence de définir la zone d'affichage de la tilemap et de la caméra en influençant la valeur de la variable globale  `scroll_x`. *Je n'entrerai pas dans le détail de cette fonctionnalité, mais je vous invite à creuser cette notion si vous voulez l'utiliser dans un de vos jeux.*

    ???- abstract "Montrer/Cacher le code"
        ```python linenums="86" 
        class Player:
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.dx = 0
                self.dy = 0
                self.direction = 1
                self.is_falling = False

            def update(self):
                global scroll_x
                last_y = self.y
                if px.btn(px.KEY_LEFT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_LEFT):
                    self.dx = -2
                    self.direction = -1
                if px.btn(px.KEY_RIGHT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_RIGHT):
                    self.dx = 2
                    self.direction = 1
                self.dy = min(self.dy + 1, 3)
                if px.btnp(px.KEY_SPACE) or px.btnp(px.GAMEPAD1_BUTTON_A):
                    self.dy = -6
                    px.play(3, 8)
                self.x, self.y = push_back(self.x, self.y, self.dx, self.dy)
                if self.x < scroll_x:
                    self.x = scroll_x
                if self.y < 0:
                    self.y = 0
                self.dx = int(self.dx * 0.8)
                self.is_falling = self.y > last_y

                if self.x > scroll_x + SCROLL_BORDER_X:
                    last_scroll_x = scroll_x
                    scroll_x = min(self.x - SCROLL_BORDER_X, 240 * 8)
                    spawn_enemy(last_scroll_x + 128, scroll_x + 127)
                if self.y >= px.height:
                    game_over()

            def draw(self):
                u = (2 if self.is_falling else px.frame_count // 3 % 2) * 8
                w = 8 if self.direction > 0 else -8
                px.blt(self.x, self.y, 0, u, 16, w, 8, TRANSPARENT_COLOR)
        ```

!!! note "Lignes 128 à 215 : Classes Ennemis"
    
    Les ennemis fonctionnent "en pilote automatique" : leur mouvement est prédéfini en fonction du type d'ennemi.

    - Ennemy1 et Ennemy2 ont besoin de détecter les murs pour faire un demi-tour (exploitation de la fonction `is_wall` et `push_back`). La direction (mouvement vers la gauche ou vers la droite) est identifiée par un attribut `direction` valant 1 ou -1.

    - Ennemy3 tire des boules de feu Ennemy3Bullet vers le Player. La variable globale `player` (instance de la classe `Player` créée dans la classe `App`) est utile à cet effet pour que Ennemy3 "sache" où se trouve le Player.

    ???- abstract "Montrer/Cacher le code"
        ```python linenums="128" 
        class Enemy1:
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.dx = 0
                self.dy = 0
                self.direction = -1
                self.is_alive = True

            def update(self):
                self.dx = self.direction
                self.dy = min(self.dy + 1, 3)
                if self.direction < 0 and is_wall(self.x - 1, self.y + 4):
                    self.direction = 1
                elif self.direction > 0 and is_wall(self.x + 8, self.y + 4):
                    self.direction = -1
                self.x, self.y = push_back(self.x, self.y, self.dx, self.dy)

            def draw(self):
                u = px.frame_count // 4 % 2 * 8
                w = 8 if self.direction > 0 else -8
                px.blt(self.x, self.y, 0, u, 24, w, 8, TRANSPARENT_COLOR)

        class Enemy2:
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.dx = 0
                self.dy = 0
                self.direction = 1
                self.is_alive = True

            def update(self):
                self.dx = self.direction
                self.dy = min(self.dy + 1, 3)
                if is_wall(self.x, self.y + 8) or is_wall(self.x + 7, self.y + 8):
                    if self.direction < 0 and (
                        is_wall(self.x - 1, self.y + 4) or not is_wall(self.x - 1, self.y + 8)
                    ):
                        self.direction = 1
                    elif self.direction > 0 and (
                        is_wall(self.x + 8, self.y + 4) or not is_wall(self.x + 7, self.y + 8)
                    ):
                        self.direction = -1
                self.x, self.y = push_back(self.x, self.y, self.dx, self.dy)

            def draw(self):
                u = px.frame_count // 4 % 2 * 8 + 16
                w = 8 if self.direction > 0 else -8
                px.blt(self.x, self.y, 0, u, 24, w, 8, TRANSPARENT_COLOR)

        class Enemy3:
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.time_to_fire = 0
                self.is_alive = True

            def update(self):
                self.time_to_fire -= 1
                if self.time_to_fire <= 0:
                    dx = player.x - self.x
                    dy = player.y - self.y
                    sq_dist = dx * dx + dy * dy
                    if sq_dist < 60**2:
                        dist = px.sqrt(sq_dist)
                        enemies.append(Enemy3Bullet(self.x, self.y, dx / dist, dy / dist))
                        self.time_to_fire = 60

            def draw(self):
                u = px.frame_count // 8 % 2 * 8
                px.blt(self.x, self.y, 0, u, 32, 8, 8, TRANSPARENT_COLOR)

        class Enemy3Bullet:
            def __init__(self, x, y, dx, dy):
                self.x = x
                self.y = y
                self.dx = dx
                self.dy = dy
                self.is_alive = True

            def update(self):
                self.x += self.dx
                self.y += self.dy

            def draw(self):
                u = px.frame_count // 2 % 2 * 8 + 16
                px.blt(self.x, self.y, 0, u, 32, 8, 8, TRANSPARENT_COLOR)
        ```

!!! note "Lignes 217 à 257 : classe principale `App`"
    Voici juste quelques indications partielles :

    - le fichier de ressource pyxres utilise des tuiles spécifiques pour prévoir les lieux d'apparition possibles des ennemis. A la ligne 223, on réécrit de façon dynamique la banque d'images avec un carré incolore à la place de ces tuiles spécifiques.

    - la variable `player` est bien défini en varaible globale pour la rendre accessible à tout le jeu.

    - dans la méthode `update`, on met fin au jeu (game over) si le joueur se trouve trop prêt d'un ennemi ; et on fait disparaître les ennemis qui sortent de la zone d'affichage (exploitation de la variable `scroll_x`).

    - la méthode `draw` exploite la fonction `camera` qui positionne le coin supérieur gauche de l'écran aux coordonnées (x, y) passées en paramètres (0, 0 par défaut) pour positionner correctement à l'écran le décor d'une part, puis les sprites d'autre part.

    ???- abstract "Montrer/Cacher le code"
        ```python linenums="217" 
        class App:
            def __init__(self):
                px.init(128, 128, title="Pyxel Platformer")
                px.load("assets/platformer.pyxres")

                # Change enemy spawn tiles invisible
                px.images[0].rect(0, 8, 24, 8, TRANSPARENT_COLOR)

                global player
                player = Player(0, 0)
                spawn_enemy(0, 127)
                px.playm(0, loop=True)
                px.run(self.update, self.draw)

            def update(self):
                if px.btn(px.KEY_Q):
                    px.quit()

                player.update()
                for enemy in enemies:
                    if abs(player.x - enemy.x) < 6 and abs(player.y - enemy.y) < 6:
                        game_over()
                        return
                    enemy.update()
                    if enemy.x < scroll_x - 8 or enemy.x > scroll_x + 160 or enemy.y > 160:
                        enemy.is_alive = False
                cleanup_entities(enemies)

            def draw(self):
                px.cls(0)

                # Draw level
                px.camera()
                px.bltm(0, 0, 0, (scroll_x // 4) % 128, 128, 128, 128)
                px.bltm(0, 0, 0, scroll_x, 0, 128, 128, TRANSPARENT_COLOR)

                # Draw characters
                px.camera(scroll_x, 0)
                player.draw()
                for enemy in enemies:
                    enemy.draw()
        ```

!!! note "Lignes 259 à 268 : Game over"
    Remise à zéro du jeu : vide la liste globale `enemies`, repositionne le joueur au début de l'écran .

    ???- abstract "Montrer/Cacher le code"
        ```python linenums="259" 
        def game_over():
            global scroll_x, enemies
            scroll_x = 0
            player.x = 0
            player.y = 0
            player.dx = 0
            player.dy = 0
            enemies = []
            spawn_enemy(0, 127)
            px.play(3, 9)
        ```