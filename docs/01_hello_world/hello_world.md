---
author: profjahier
title: Tuto Hello World
---

# Hello World

## La version officielle (01_hello_pyxel.py)
[Tester online :fontawesome-solid-paper-plane:](https://kitao.github.io/pyxel/wasm/examples/01_hello_pyxel.html){.md-button target=_blank}

!!! abstract "[Voir le code originel](https://github.com/kitao/pyxel/blob/main/python/pyxel/examples/01_hello_pyxel.py){target=_blank}  sur Github"

##  Le code en détail (sans POO)
Ce tutoriel ciblant aussi des élèves de 1ère NSI, je propose d'abord une version un peu modifiée du programme n'exploitant pas la Programmation Orientée Objet (POO) :
!!! abstract ""
    ```python title="Code source : jeu.py" linenums="1"
    import pyxel as px

    px.init(160, 120, title="Hello pyxel")
    px.images[0].load(0, 0, "assets/logo.png") # nécessite cette image !

    def update():
        if px.btnp(px.KEY_Q):
            px.quit()

    def draw():
        px.cls(0)
        px.text(55, 41, "Hello, pyxel!", px.frame_count % 16)
        px.blt(61, 66, 0, 0, 0, 38, 16)

    px.run(update, draw)
    ```

Outre les fonctions basiques `run` `update` et `draw`, on trouve quelques fonctionnalités complémentaires :

- ligne 4 : `#!python px.images[0].load(0, 0, "assets/logo.png")` : charge l'image *logo.png* dans la banque d'images n°0 de pyxel, aux coordonnées *x, y = 0, 0* de cette banque d'images.
!!! tip "Banques d'images" 
    `px.images` est la **liste** des banques d'images  de pyxel (indicées de 0 à 2).

    `#!python px.images[0]` est un objet de la classe `Image` qui possède une méthode `load`.

- ligne 8 : `#!python px.btnp(px.KEY_Q)` détecte l'appui sur la touche <kbd>Q</kbd>. Il existe aussi les fonctions voisines `btn` et `btnr` (voir la [doc officielle](https://github.com/kitao/pyxel/blob/main/docs/README.fr.md#entr%C3%A9es){target=_blank} pour plus d'infos).

- ligne 9 : `#!python px.quit()` : met un terme au jeu !

- ligne 12 : `#!python px.cls(0)` (**cl**ear**s**creen) : colorie l'ensemble de l'écran de jeu de la couleur n°0.
!!! tip "Couleurs"
    pyxel possède une palette de 16 couleurs numérotées de 0 à 15. [La palette est décrite ici](https://github.com/kitao/pyxel/blob/main/docs/README.fr.md#palette-de-couleurs){target=_blank}. (*remarque :* Il est possible de modifier cette palette par défaut)

- ligne 13 : `#!python px.text(55, 41, "Hello, pyxel!", px.frame_count % 16)` affiche le texte "Hello, pyxel!" aux coordonnées de l'écran *(x , y = 55, 41)*. Le dernier paramètre est la couleur du texte (entier entre 0 et 15 de la palette). Ici cette couleur est variable car elle est définie par la valeur `px.frame_count % 16` (rappelons que `px.frame_count` contient le numéro de l'image en cours -- incrémenté au rythme de *fps*).
 
- ligne 14 : `#!python px.blt(61, 66, 0, 0, 0, 38, 16)` applique une portion choisie d'une banque d'images sur l'écran de jeu. 

    Voici une explication plus précise de ses paramètres : `#!python px.blt(x_ecran, y_ecran, banque_n°, x_banque, y_banque, largeur, hauteur)`  

    - `x_ecran, y_ecran` : coordonnées (coin sup. gauche) sur l'écran de jeu où appliquer l'image.

    - `banque_n°` : indice désignant la banque d'images (entre 0 et 2).

    - `x_banque, y_banque` : coordonnées (coin sup. gauche) de la portion d'image de la banque à appliquer. 

    - `largeur, hauteur` : largeur et hauteur de la portion d'image de la banque à appliquer. 

    Concrètement, dans le code de ce jeu, on applique sur l'écran aux coordonnées *(61, 66)*, une portion d'image de la banque 0, prise aux coordonnées *(0, 0)* de cette banque, sur une largeur et une hauteur de *38 x 16* px (c'est l'image *logo.png* qu'on avait préalablement chargé dans cette banque).

## Utlisation d'un fichier pyxres

Le code proposé originellement importe et charge une image d'un fichier annexe pour le logo.
Il est aussi possible de manipuler directement les banques d'images (ainsi que les banques de sons) dans un fichier spécial de type **.pyxres*.

En supposant qu'on ait créé à la main un logo de 38 x 16 px dans ce fichier (à partir de la position (0, 0)), nommé *res.pyxres*, alors il suffit de changer la ligne 4 du code de la façon suivante :
!!! abstract ""
    ```python   linenums="4" 
    #px.images[0].load(0, 0, "assets/logo.png") # ligne désormais inutile
    px.load("res.pyxres")                       # remplacée par celle-ci
    ```
Voici une capture de l'édition du fichier *res.pyres* :

![res.pyres](pyxres.png){width=400}

Vous pouvez télécharger ce fichier depuis le [lien public du projet](https://www.pyxelstudio.net/bj963z2p) sur le site **pyxelstudio**.

##  La version POO
La version en Programmation Orientée Objet (*niveau cours de Terminale NSI*) ne pose guère de problème d'adaptation.

Notons simplement que les fonctions `update` et `draw` sont des méthodes de la classe `App` et que la fonction `run` est appelée directement dans la fonction `__init__` de cette classe, qui n'est instantiée qu'une seule fois (ligne 18 du code) pour démarrer l'application de façon effective.
!!! abstract ""
    ```python title="Code source POO : jeu.py" linenums="1"
    import pyxel as px

    class App:
        def __init__(self):
            px.init(160, 120, title="Hello pyxel")
            px.images[0].load(0, 0, "assets/logo.png") # nécessite cette image !
            px.run(self.update, self.draw)

        def update(self):
            if px.btnp(px.KEY_Q):
                px.quit()

        def draw(self):
            px.cls(0)
            px.text(55, 41, "Hello, pyxel!", px.frame_count % 16)
            px.blt(61, 66, 0, 0, 0, 38, 16)

    App()
    ```

