---
author: profjahier
title: Tuto Shooter
---

# Shooter

## La version officielle (09_shooter.py)
[Tester online :fontawesome-solid-paper-plane:](https://kitao.github.io/pyxel/wasm/examples/09_shooter.html){.md-button target=_blank}

!!! abstract "[Voir le code originel](https://github.com/kitao/pyxel/blob/main/python/pyxel/examples/09_shooter.py){target=_blank}  sur Github"

Avec cet exemple, nous allons à nouveau nous focaliser sur quelques bonnes pratiques de programmation pour coder un jeu pyxel.

## Utilisation de variables globales
Lorsqu'on n'utilise pas le paradigme de la Programmation Orientée Objet, on a souvent besoin d'utiliser des variables globales pour partager des informations d'une fonction à une autre.

Rappelons ici quelques notions sur les **espaces de noms** (de façon relativement pragmatique sans être trop théorique). 

Lorsqu'une variable apparaît dans une instruction d'une fonction, celle-ci est a priori considérée comme locale, et l'interpréteur Python cherche dans l'espace des noms de cette fonction sa valeur (quelque part dans une instruction antérieure). Si cette variable n'a pas été définie antérieurement dans la fonction, elle ne fait donc pas partie de l'espace de noms de la fonction, et l'interpréteur Python recherche alors dans l'espace des noms d'où a été appelée la fonction (ce peut être une autre fonction, ou l'espace global du programme principal) la présence de cette variable. *(si cette variable n'a jamais été définie, une exception "NameError: name '?' is not defined" est levée)*.

Ce fonctionnement permet de partager une variable globale (définie au niveau du programme principal) entre les fonctions. Cette variable sera en effet visible dans toutes les fonctions.

!!! tip "Mot clef `global`"
    Si une fonction accède à une variable globale `var` et souhaite modifier le contenu qu'elle référence pour que cette modification reste perçue au niveau du programme principal lorsque la fonction se termine, il faut écrire l'instruction `#!py global var` dans la fonction !

    !!! example "Exemple" 
    ```python linenums="1"
    def modif():
        global a
        a = 10
        b = 'louis'

    a = 5
    b = 'henri'
    modif()
    ```
    Après l'appel `modif()` à la ligne 8, dans le programme principal la variable `a`vaut désormais 10 ; en revanche `b` vaut toujours `#!py 'henri'`.

Vous pouvez exécuter ces instructions dans la console ci-dessous et faire d'autres tests à votre gré.
{{ IDE('variable_globale') }}

!!! note "Le cas particulier des listes Python (type `list`)"
    La modification d'une liste dans une fonction se répercute automatiquement dans le programme principal sans l'utilisation du mot clef `#!py global` car la modification d'une liste s'effectue **en place**.
    {{ IDE('liste') }}

Venons-en désormais au patron de programmation qui exploite ces variables globales dans le code d'un jeu pyxel. En début de programme principal, on définit un ensemble de variables globales utiles au jeu (comme un compteur de points, ou un identifiant de scènes de jeu, ou encore une liste contenant l'ensemble des ennemis, etc) qui seront utilement consultées et, le cas échéant, mises à jour dans les fonctions `update`.

Même si cette pratique est généralement moins fréquente lorsqu'on utilise la POO, ici cela reste une manière très pratique de partager des infos entre les différentes classes définissant les entités du jeu (héros, ennemis, tirs animés...)


## Code source brut selon la doc officielle
On pourra lire ici le code complet sans commentaire. La section suivante présente des commentaires assez généraux permettant de mieux comprendre les étapes de ce code. Je conseille de lire les deux en parallèle.
???- abstract "Code source"
    ```python title="jeu.py" linenums="1"
    import pyxel as px

    SCENE_TITLE = 0
    SCENE_PLAY = 1
    SCENE_GAMEOVER = 2

    NUM_STARS = 100
    STAR_COLOR_HIGH = 12
    STAR_COLOR_LOW = 5

    PLAYER_WIDTH = 8
    PLAYER_HEIGHT = 8
    PLAYER_SPEED = 2

    BULLET_WIDTH = 2
    BULLET_HEIGHT = 8
    BULLET_COLOR = 11
    BULLET_SPEED = 4

    ENEMY_WIDTH = 8
    ENEMY_HEIGHT = 8
    ENEMY_SPEED = 1.5

    BLAST_START_RADIUS = 1
    BLAST_END_RADIUS = 8
    BLAST_COLOR_IN = 7
    BLAST_COLOR_OUT = 10

    enemies = []
    bullets = []
    blasts = []

    def update_entities(entities):
        for entity in entities:
            entity.update()

    def draw_entities(entities):
        for entity in entities:
            entity.draw()

    def cleanup_entities(entities):
        for i in range(len(entities) - 1, -1, -1):
            if not entities[i].is_alive:
                del entities[i]

    def load_bgm(msc, filename, snd1, snd2, snd3):
        # Loads a json file for 8bit BGM generator by frenchbread.
        # Each track is stored in snd1, snd2 and snd3 of the sound
        # respectively and registered in msc of the music.
        import json

        with open(filename, "rt") as file:
            bgm = json.loads(file.read())
            px.sounds[snd1].set(*bgm[0])
            px.sounds[snd2].set(*bgm[1])
            px.sounds[snd3].set(*bgm[2])
            px.musics[msc].set([snd1], [snd2], [snd3])

    class Background:
        def __init__(self):
            self.stars = []
            for i in range(NUM_STARS):
                self.stars.append(
                    (
                        px.rndi(0, px.width - 1),
                        px.rndi(0, px.height - 1),
                        px.rndf(1, 2.5),
                    )
                )

        def update(self):
            for i, (x, y, speed) in enumerate(self.stars):
                y += speed
                if y >= px.height:
                    y -= px.height
                self.stars[i] = (x, y, speed)

        def draw(self):
            for x, y, speed in self.stars:
                px.pset(x, y, STAR_COLOR_HIGH if speed > 1.8 else STAR_COLOR_LOW)

    class Player:
        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.w = PLAYER_WIDTH
            self.h = PLAYER_HEIGHT
            self.is_alive = True

        def update(self):
            if px.btn(px.KEY_LEFT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_LEFT):
                self.x -= PLAYER_SPEED
            if px.btn(px.KEY_RIGHT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_RIGHT):
                self.x += PLAYER_SPEED
            if px.btn(px.KEY_UP) or px.btn(px.GAMEPAD1_BUTTON_DPAD_UP):
                self.y -= PLAYER_SPEED
            if px.btn(px.KEY_DOWN) or px.btn(px.GAMEPAD1_BUTTON_DPAD_DOWN):
                self.y += PLAYER_SPEED
            self.x = max(self.x, 0)
            self.x = min(self.x, px.width - self.w)
            self.y = max(self.y, 0)
            self.y = min(self.y, px.height - self.h)

            if px.btnp(px.KEY_SPACE) or px.btnp(px.GAMEPAD1_BUTTON_A):
                Bullet(
                    self.x + (PLAYER_WIDTH - BULLET_WIDTH) / 2, self.y - BULLET_HEIGHT / 2
                )
                px.play(3, 0)

        def draw(self):
            px.blt(self.x, self.y, 0, 0, 0, self.w, self.h, 0)

    class Bullet:
        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.w = BULLET_WIDTH
            self.h = BULLET_HEIGHT
            self.is_alive = True
            bullets.append(self)

        def update(self):
            self.y -= BULLET_SPEED
            if self.y + self.h - 1 < 0:
                self.is_alive = False

        def draw(self):
            px.rect(self.x, self.y, self.w, self.h, BULLET_COLOR)

    class Enemy:
        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.w = ENEMY_WIDTH
            self.h = ENEMY_HEIGHT
            self.dir = 1
            self.timer_offset = px.rndi(0, 59)
            self.is_alive = True
            enemies.append(self)

        def update(self):
            if (px.frame_count + self.timer_offset) % 60 < 30:
                self.x += ENEMY_SPEED
                self.dir = 1
            else:
                self.x -= ENEMY_SPEED
                self.dir = -1
            self.y += ENEMY_SPEED
            if self.y > px.height - 1:
                self.is_alive = False

        def draw(self):
            px.blt(self.x, self.y, 0, 8, 0, self.w * self.dir, self.h, 0)

    class Blast:
        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.radius = BLAST_START_RADIUS
            self.is_alive = True
            blasts.append(self)

        def update(self):
            self.radius += 1
            if self.radius > BLAST_END_RADIUS:
                self.is_alive = False

        def draw(self):
            px.circ(self.x, self.y, self.radius, BLAST_COLOR_IN)
            px.circb(self.x, self.y, self.radius, BLAST_COLOR_OUT)

    class App:
        def __init__(self):
            px.init(120, 160, title="Pyxel Shooter")
            px.images[0].set(
                0,
                0,
                [
                    "00c00c00",
                    "0c7007c0",
                    "0c7007c0",
                    "c703b07c",
                    "77033077",
                    "785cc587",
                    "85c77c58",
                    "0c0880c0",
                ],
            )
            px.images[0].set(
                8,
                0,
                [
                    "00088000",
                    "00ee1200",
                    "08e2b180",
                    "02882820",
                    "00222200",
                    "00012280",
                    "08208008",
                    "80008000",
                ],
            )
            px.sounds[0].set("a3a2c1a1", "p", "7", "s", 5)
            px.sounds[1].set("a3a2c2c2", "n", "7742", "s", 10)
            load_bgm(0, "assets/bgm_title.json", 2, 3, 4)
            load_bgm(1, "assets/bgm_play.json", 5, 6, 7)
            self.scene = SCENE_TITLE
            self.score = 0
            self.background = Background()
            self.player = Player(px.width / 2, px.height - 20)
            px.playm(0, loop=True)
            px.run(self.update, self.draw)

        def update(self):
            if px.btn(px.KEY_Q):
                px.quit()

            self.background.update()
            if self.scene == SCENE_TITLE:
                self.update_title_scene()
            elif self.scene == SCENE_PLAY:
                self.update_play_scene()
            elif self.scene == SCENE_GAMEOVER:
                self.update_gameover_scene()

        def update_title_scene(self):
            if px.btnp(px.KEY_RETURN) or px.btnp(px.GAMEPAD1_BUTTON_X):
                self.scene = SCENE_PLAY
                px.playm(1, loop=True)

        def update_play_scene(self):
            if px.frame_count % 6 == 0:
                Enemy(px.rndi(0, px.width - ENEMY_WIDTH), 0)

            for enemy in enemies:
                for bullet in bullets:
                    if (
                        enemy.x + enemy.w > bullet.x
                        and bullet.x + bullet.w > enemy.x
                        and enemy.y + enemy.h > bullet.y
                        and bullet.y + bullet.h > enemy.y
                    ):
                        enemy.is_alive = False
                        bullet.is_alive = False
                        blasts.append(
                            Blast(enemy.x + ENEMY_WIDTH / 2, enemy.y + ENEMY_HEIGHT / 2)
                        )
                        px.play(3, 1)
                        self.score += 10

            for enemy in enemies:
                if (
                    self.player.x + self.player.w > enemy.x
                    and enemy.x + enemy.w > self.player.x
                    and self.player.y + self.player.h > enemy.y
                    and enemy.y + enemy.h > self.player.y
                ):
                    enemy.is_alive = False
                    blasts.append(
                        Blast(
                            self.player.x + PLAYER_WIDTH / 2,
                            self.player.y + PLAYER_HEIGHT / 2,
                        )
                    )
                    px.play(3, 1)
                    self.scene = SCENE_GAMEOVER
                    px.playm(0, loop=True)

            self.player.update()
            update_entities(bullets)
            update_entities(enemies)
            update_entities(blasts)
            cleanup_entities(enemies)
            cleanup_entities(bullets)
            cleanup_entities(blasts)

        def update_gameover_scene(self):
            update_entities(bullets)
            update_entities(enemies)
            update_entities(blasts)
            cleanup_entities(enemies)
            cleanup_entities(bullets)
            cleanup_entities(blasts)

            if px.btnp(px.KEY_RETURN) or px.btnp(px.GAMEPAD1_BUTTON_X):
                self.scene = SCENE_PLAY
                self.player.x = px.width / 2
                self.player.y = px.height - 20
                self.score = 0
                enemies.clear()
                bullets.clear()
                blasts.clear()
                px.playm(1, loop=True)

        def draw(self):
            px.cls(0)
            self.background.draw()
            if self.scene == SCENE_TITLE:
                self.draw_title_scene()
            elif self.scene == SCENE_PLAY:
                self.draw_play_scene()
            elif self.scene == SCENE_GAMEOVER:
                self.draw_gameover_scene()
            px.text(39, 4, f"SCORE {self.score:5}", 7)

        def draw_title_scene(self):
            px.text(35, 66, "Pyxel Shooter", px.frame_count % 16)
            px.text(31, 126, "- PRESS ENTER -", 13)

        def draw_play_scene(self):
            self.player.draw()
            draw_entities(bullets)
            draw_entities(enemies)
            draw_entities(blasts)

        def draw_gameover_scene(self):
            draw_entities(bullets)
            draw_entities(enemies)
            draw_entities(blasts)
            px.text(43, 66, "GAME OVER", 8)
            px.text(31, 126, "- PRESS ENTER -", 13)

    App()
    ```

## Quelques commentaires sur le code
Ci-dessous, je propose de parcourir l'ensemble du code en indiquant simplement les quelques points d'importance à bien identifer (décrit dans l'ordre d'écriture du code) :

!!! note "Lignes 3 à 27 : paramétrages globaux"
    En début de programme, on définit plusieurs constantes (*écrites en majuscules*) en lien avec des paramètres de jeu. Plusieurs valeurs numériques (comme les dimensions par exemple) sont en lien direct avec la banque d'images définissant les images du jeu.
    ???- abstract "Montrer/Cacher le code"
        ```python   linenums="3" 
        SCENE_TITLE = 0
        SCENE_PLAY = 1
        SCENE_GAMEOVER = 2

        NUM_STARS = 100
        STAR_COLOR_HIGH = 12
        STAR_COLOR_LOW = 5

        PLAYER_WIDTH = 8
        PLAYER_HEIGHT = 8
        PLAYER_SPEED = 2

        BULLET_WIDTH = 2
        BULLET_HEIGHT = 8
        BULLET_COLOR = 11
        BULLET_SPEED = 4

        ENEMY_WIDTH = 8
        ENEMY_HEIGHT = 8
        ENEMY_SPEED = 1.5

        BLAST_START_RADIUS = 1
        BLAST_END_RADIUS = 8
        BLAST_COLOR_IN = 7
        BLAST_COLOR_OUT = 10 
        ```
!!! note "Lignes 29 à 31 : définitions de variables globales" 
    On retrouve le patron de programmation proposé dans ce tutoriel. Ces variables permettent de partager de l'information dans tout le code.
    Ici les listes vont contenir les entités créées en cours de jeu.
    ???- abstract "Montrer/Cacher le code"
        ```python   linenums="29" 
        enemies = []
        bullets = []
        blasts = []
        ``` 

!!! note "Lignes 33 à 44 : fonctions `update_entities`, `draw_entities` et `cleanup_entities`" 
    Ces fonctions gérent les différentes entités du jeu (vaisseau héros, ennemis, tirs, explosions). Chacune de ces entités est modélisée par une classe qui implémente ses propres méthode `update` et `draw`. La fonction `clean_up` s'appuie sur le booléen `is_alive` d'une entité pour la supprimer si nécessaire (*remarque : les entités sont supprimées par la fin de la liste avec un range décroissant*). 
    ???- abstract "Montrer/Cacher le code"
        ```python   linenums="33" 
        def update_entities(entities):
            for entity in entities:
                entity.update()

        def draw_entities(entities):
            for entity in entities:
                entity.draw()

        def cleanup_entities(entities):
            for i in range(len(entities) - 1, -1, -1):
                if not entities[i].is_alive:
                    del entities[i]
        ``` 

!!! note "Lignes 46 à 57 : fonction `load_bgm`" 
    Cette fonction, en lien avec le son, ne nous intéresse pas dans un 1er temps.
    ???- abstract "Montrer/Cacher le code"
        ```python   linenums="46" 
        def load_bgm(msc, filename, snd1, snd2, snd3):
            # Loads a json file for 8bit BGM generator by frenchbread.
            # Each track is stored in snd1, snd2 and snd3 of the sound
            # respectively and registered in msc of the music.
            import json

            with open(filename, "rt") as file:
                bgm = json.loads(file.read())
                px.sounds[snd1].set(*bgm[0])
                px.sounds[snd2].set(*bgm[1])
                px.sounds[snd3].set(*bgm[2])
                px.musics[msc].set([snd1], [snd2], [snd3])
        ```  

!!! note "Lignes 59 à 80 : classe `Background`" 
    Permet de générer les étoiles de fond de décors et gère leur déplacement à l'écran. Chaque étoile est enregistrée dans la liste `stars` qui est un attribut de cette classe. Cette classe ne sera instanciée qu'une seule fois dans la classe principale du jeu `App`.
    ???- abstract "Montrer/Cacher le code"
        ```python   linenums="59" 
        class Background:
            def __init__(self):
                self.stars = []
                for i in range(NUM_STARS):
                    self.stars.append(
                        (
                            px.rndi(0, px.width - 1),
                            px.rndi(0, px.height - 1),
                            px.rndf(1, 2.5),
                        )
                    )

            def update(self):
                for i, (x, y, speed) in enumerate(self.stars):
                    y += speed
                    if y >= px.height:
                        y -= px.height
                    self.stars[i] = (x, y, speed)

            def draw(self):
                for x, y, speed in self.stars:
                    px.pset(x, y, STAR_COLOR_HIGH if speed > 1.8 else STAR_COLOR_LOW)
        ```   

!!! note "Lignes 82 à 111 : classe `Player`" 
    Notez l'instanciation d'une `Bullet` lignes 105 à 107 (tir déclenché par la barre espace). Lorsqu'un tir est instancié, il démarre alors une "existence" indépendante dont le processus est défini dans sa classe. `PLAYER_SPEED` est un des paramètres généraux définis au début du code permettant de déterminer la valeur du déplacement du vaisseau entre deux frames.
    ???- abstract "Montrer/Cacher le code"
        ```python   linenums="82" 
        class Player:
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.w = PLAYER_WIDTH
                self.h = PLAYER_HEIGHT
                self.is_alive = True

            def update(self):
                if px.btn(px.KEY_LEFT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_LEFT):
                    self.x -= PLAYER_SPEED
                if px.btn(px.KEY_RIGHT) or px.btn(px.GAMEPAD1_BUTTON_DPAD_RIGHT):
                    self.x += PLAYER_SPEED
                if px.btn(px.KEY_UP) or px.btn(px.GAMEPAD1_BUTTON_DPAD_UP):
                    self.y -= PLAYER_SPEED
                if px.btn(px.KEY_DOWN) or px.btn(px.GAMEPAD1_BUTTON_DPAD_DOWN):
                    self.y += PLAYER_SPEED
                self.x = max(self.x, 0)
                self.x = min(self.x, px.width - self.w)
                self.y = max(self.y, 0)
                self.y = min(self.y, px.height - self.h)

                if px.btnp(px.KEY_SPACE) or px.btnp(px.GAMEPAD1_BUTTON_A):
                    Bullet(
                        self.x + (PLAYER_WIDTH - BULLET_WIDTH) / 2, self.y - BULLET_HEIGHT / 2
                    )
                    px.play(3, 0)

            def draw(self):
                px.blt(self.x, self.y, 0, 0, 0, self.w, self.h, 0)
        ```   
 
!!! note "Lignes 113 à 170 : classes `Bullet`, `Enemy` et `Blast`" 
    Ici, on note l'interêt des variables globales définies en début de programme (listes `enemies`, `bullets` et  `blasts`) auxquelles on ajoute ces entités avec les instructions telles que `enemies.append(self)` par exemple. Remarquons aussi l'attribut `is_alive` de chacune de ces classes permettant de contrôler que les entités restent actives dans le jeu ou non.

    La classe `Blast` (explosion) est instanciée dans la classe principal `App` lorsqu'un ennemi est touché par un tir ou bien lorsque le joueur est touché par un ennemi (cela mettant d'ailleurs fin au jeu et enclenche un changement de scène).
    ???- abstract "Montrer/Cacher le code"
        ```python   linenums="113" 
        class Bullet:
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.w = BULLET_WIDTH
                self.h = BULLET_HEIGHT
                self.is_alive = True
                bullets.append(self)

            def update(self):
                self.y -= BULLET_SPEED
                if self.y + self.h - 1 < 0:
                    self.is_alive = False

            def draw(self):
                px.rect(self.x, self.y, self.w, self.h, BULLET_COLOR)

        class Enemy:
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.w = ENEMY_WIDTH
                self.h = ENEMY_HEIGHT
                self.dir = 1
                self.timer_offset = px.rndi(0, 59)
                self.is_alive = True
                enemies.append(self)

            def update(self):
                if (px.frame_count + self.timer_offset) % 60 < 30:
                    self.x += ENEMY_SPEED
                    self.dir = 1
                else:
                    self.x -= ENEMY_SPEED
                    self.dir = -1
                self.y += ENEMY_SPEED
                if self.y > px.height - 1:
                    self.is_alive = False

            def draw(self):
                px.blt(self.x, self.y, 0, 8, 0, self.w * self.dir, self.h, 0)

        class Blast:
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.radius = BLAST_START_RADIUS
                self.is_alive = True
                blasts.append(self)

            def update(self):
                self.radius += 1
                if self.radius > BLAST_END_RADIUS:
                    self.is_alive = False

            def draw(self):
                px.circ(self.x, self.y, self.radius, BLAST_COLOR_IN)
                px.circb(self.x, self.y, self.radius, BLAST_COLOR_OUT)
        ```    
!!! note "Lignes 172 à 321 :  classe principale `App` qui gère l'ensemble du jeu" 
    !!! note "Lignes 173 à 212 : Initialisation (méthode `__init__`)"
        - L'initialisation commence par charger des images et des sons dans la banque d'images et de sons. Cela pourrait être évité en utilisant un fichier de ressources pyxres.
        - un attribut `scene` permettra de suivra l'état global du jeu pour savoir quelles mises à jours faire ou non, et quels éléments graphiques dessiner en fonction de la scène de jeu. On note au passage l'utilisation des paramètrs globaux du jeu défini au tout début (ex : `SCENE_TITLE`)
        - un fond de décors et un joueur sont instanciés
        - puis la musique est activée
        - enfin, la fonction `run` déclenche la boucle principale
        ???- abstract "Montrer/Cacher le code"
            ```python   linenums="173" 
            def __init__(self):
                px.init(120, 160, title="Pyxel Shooter")
                px.images[0].set(
                    0,
                    0,
                    [
                        "00c00c00",
                        "0c7007c0",
                        "0c7007c0",
                        "c703b07c",
                        "77033077",
                        "785cc587",
                        "85c77c58",
                        "0c0880c0",
                    ],
                )
                px.images[0].set(
                    8,
                    0,
                    [
                        "00088000",
                        "00ee1200",
                        "08e2b180",
                        "02882820",
                        "00222200",
                        "00012280",
                        "08208008",
                        "80008000",
                    ],
                )
                px.sounds[0].set("a3a2c1a1", "p", "7", "s", 5)
                px.sounds[1].set("a3a2c2c2", "n", "7742", "s", 10)
                load_bgm(0, "assets/bgm_title.json", 2, 3, 4)
                load_bgm(1, "assets/bgm_play.json", 5, 6, 7)
                self.scene = SCENE_TITLE
                self.score = 0
                self.background = Background()
                self.player = Player(px.width / 2, px.height - 20)
                px.playm(0, loop=True)
                px.run(self.update, self.draw)
            ```     
            
    !!! note "Lignes 214 à 224 : fonction `update`"
        On constate l'utilité des de l'attribut `scene` pour le choix des mises à jour (couplées aux méthodes `update_title_scene`,  `update_play_scene` et `update_gameover_scene`)
        ???- abstract "Montrer/Cacher le code"
            ```python   linenums="214" 
            def update(self):
                if px.btn(px.KEY_Q):
                    px.quit()

                self.background.update()
                if self.scene == SCENE_TITLE:
                    self.update_title_scene()
                elif self.scene == SCENE_PLAY:
                    self.update_play_scene()
                elif self.scene == SCENE_GAMEOVER:
                    self.update_gameover_scene()
            ```     
    !!! note "Lignes 226 à 229 : fonction `update_title_scene`"
        Propose de démarrer le jeu par l'appui sur <kbd>Entrée</kbd> en mettant à jour l'attribut `scene` à `SCENE_PLAY`.
        ???- abstract "Montrer/Cacher le code"
            ```python   linenums="226" 
            def update_title_scene(self):
                if px.btnp(px.KEY_RETURN) or px.btnp(px.GAMEPAD1_BUTTON_X):
                    self.scene = SCENE_PLAY
                    px.playm(1, loop=True)
            ```     
    !!! note "Lignes 231 à 275 : fonction `update_play_scene`"
        Génère des ennemis régulièrement (on retrouve une utilisation de `px.frame_count % 6 == 0` déjà analysée dans l'exemple [Jump Game](../../02_jump_game/jump_game#astuces-pour-les-affichages-pxframe_count-p-n).) C'est aussi dans cette fonction que les tests de collisions entre les entités sont analysés, entrainant les conséquences qui en découlent.
        ???- abstract "Montrer/Cacher le code"
            ```python   linenums="231" 
            def update_play_scene(self):
                if px.frame_count % 6 == 0:
                    Enemy(px.rndi(0, px.width - ENEMY_WIDTH), 0)

                for enemy in enemies:
                    for bullet in bullets:
                        if (
                            enemy.x + enemy.w > bullet.x
                            and bullet.x + bullet.w > enemy.x
                            and enemy.y + enemy.h > bullet.y
                            and bullet.y + bullet.h > enemy.y
                        ):
                            enemy.is_alive = False
                            bullet.is_alive = False
                            blasts.append(
                                Blast(enemy.x + ENEMY_WIDTH / 2, enemy.y + ENEMY_HEIGHT / 2)
                            )
                            px.play(3, 1)
                            self.score += 10

                for enemy in enemies:
                    if (
                        self.player.x + self.player.w > enemy.x
                        and enemy.x + enemy.w > self.player.x
                        and self.player.y + self.player.h > enemy.y
                        and enemy.y + enemy.h > self.player.y
                    ):
                        enemy.is_alive = False
                        blasts.append(
                            Blast(
                                self.player.x + PLAYER_WIDTH / 2,
                                self.player.y + PLAYER_HEIGHT / 2,
                            )
                        )
                        px.play(3, 1)
                        self.scene = SCENE_GAMEOVER
                        px.playm(0, loop=True)

                self.player.update()
                update_entities(bullets)
                update_entities(enemies)
                update_entities(blasts)
                cleanup_entities(enemies)
                cleanup_entities(bullets)
                cleanup_entities(blasts)
            ```     
    
    !!! note "Lignes 277 à 293 : fonction `update_gameover_scene`"
        Propose de redémarrer le jeu par l'appui sur <kbd>Entrée</kbd> en mettant à jour l'attribut `scene` à `SCENE_PLAY`.
        ???- abstract "Montrer/Cacher le code"
            ```python   linenums="277" 
            def update_gameover_scene(self):
                update_entities(bullets)
                update_entities(enemies)
                update_entities(blasts)
                cleanup_entities(enemies)
                cleanup_entities(bullets)
                cleanup_entities(blasts)

                if px.btnp(px.KEY_RETURN) or px.btnp(px.GAMEPAD1_BUTTON_X):
                    self.scene = SCENE_PLAY
                    self.player.x = px.width / 2
                    self.player.y = px.height - 20
                    self.score = 0
                    enemies.clear()
                    bullets.clear()
                    blasts.clear()
                    px.playm(1, loop=True)
            ```     
    
    !!! note "Lignes 295 à 321 : pour finir, les fonctions d'affichages (`draw`)"
        Ces fonctions ne doivent pas poser de difficultés particulières.
        ???- abstract "Montrer/Cacher le code"
            ```python   linenums="295" 
            def draw(self):
                px.cls(0)
                self.background.draw()
                if self.scene == SCENE_TITLE:
                    self.draw_title_scene()
                elif self.scene == SCENE_PLAY:
                    self.draw_play_scene()
                elif self.scene == SCENE_GAMEOVER:
                    self.draw_gameover_scene()
                px.text(39, 4, f"SCORE {self.score:5}", 7)

            def draw_title_scene(self):
                px.text(35, 66, "Pyxel Shooter", px.frame_count % 16)
                px.text(31, 126, "- PRESS ENTER -", 13)

            def draw_play_scene(self):
                self.player.draw()
                draw_entities(bullets)
                draw_entities(enemies)
                draw_entities(blasts)

            def draw_gameover_scene(self):
                draw_entities(bullets)
                draw_entities(enemies)
                draw_entities(blasts)
                px.text(43, 66, "GAME OVER", 8)
                px.text(31, 126, "- PRESS ENTER -", 13)
            ```     
   
  