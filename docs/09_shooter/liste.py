 
# --------- PYODIDE:code --------- #

liste = [1, 2, 3]

def modif():
    liste[0] = 50
    liste.append(4)


modif()
print("Après appel à modif(), dans le programme principal liste vaut :", liste)
 
# --------- PYODIDE:tests --------- #

assert liste == [50, 2, 3, 4]
 