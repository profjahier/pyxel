 
# --------- PYODIDE:code --------- #

def modif():
    global a
    a = 10
    b = 'louis'
    print("Après modification, dans la fonction :", a, b)
    
a = 5
b = 'henri'

print("définition initiale :", a, b)
modif()
print("Après appel à modif(), dans le programme principal :", a, b)
 
# --------- PYODIDE:tests --------- #

assert a == 10
assert b == 'henri'
 