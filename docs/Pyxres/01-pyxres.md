---
author: profjahier
title: Fichier de ressources pyxres
---

# Fichier de ressources pyxres

Un fichier de ressources pyxres permet d'enregistrer des images et des sons utiles au jeu qu'on développe.

Si pyxres est installé sur votre machine, vous pouvez éditer un fichier pyxres avec la commande `pyxel edit nom_fichier.pyxres`

Dans l'interface de [pyxelstudio](https://pyxelstudio.net){target=_blank}, il y a une icône dédiée.

Lorsque le fichier de ressources est édité, on peut accéder à 4 "sous-éditeurs" différents :

- éditeur de banques d'images
- éditeur de tilemaps (carte globale du jeu combinant des images de la banque d'images)
- éditeur de sons
- éditeur de musiques (combinant des sons de la banque de sons)

Voici une copie d'écran des "sous-éditeurs" d'un exemple de fichier pyxres :

![éditeur banque d'images](banq_img.png){width=300}
![éditeur tilemaps](tilemap.png){width=300}

![éditeur banque de sons](banq_son.png){width=300}
![éditeur musiques](musique.png){width=300}

## Étude détaillée des images et tilemaps

- **Banques d'images :**

La création d'images ne doit pas poser trop de difficultés (l'éditeur est assez explicite). On peut choisir une couleur de la palette, créer des formes pixel par pixel, ou bien des cercles ou rectangles (pleins ou vides), et même utiliser un outil de remplissage de contour. Il est aussi possible de copier/couper/coller des portions d'images.

La zone de dessin de l'éditeur présente une portion de la banque de 16 x 16 pixels, mais il est possible de naviguer ailleurs dans la banque. La taille d'une banque complète est de 256 x 256 pixels. Les coordonnées du curseur au sein de la banque est toujours écrite sous forme d'un couple (x, y) dans la barre d'info de l'éditeur.

La 1ère coordonnée (x) est horizontale et augmente de gauche à droite (de 0 à 255). La 2nde coordonnée (y) est verticale et augmente de haut en bas (de 0 à 255). L'origine des coordonnées est donc le coin supérieur gauche.

Notons enfin qu'il existe 3 banques disponibles, indicées de 0 à 2. Dans un programme, la liste des banques d'images est accessible avec la variable `px.images`, et la banque n°i est donc accessible par `px.images[i]`.


- **Tilemaps (carte de tuiles) :**

Une tilemap est une zone graphique combinant des portions d'images d'une banque d'images. On l'utilise en général pour construire un décor de jeu.

Pour construire une tilemap, on sélectionne une zone de la banque d'images que l'on applique quelque part sur la tilemap. La plus petite zone sélectionnable de la banque d'images est un carré de 8 x 8 pixels (c'est une **tuile**), mais on peut sélectionner des zones plus grandes.

Pour dessiner dans la tilemap, on retrouve les mêmes outils que pour la banque d'images (points, cercles, rectangles...), mais au lieu de dessiner des pixels d'une couleur de la palette, on dessine avec les tuiles de la banque (les images "élémentaires" de 8 x 8 pixels).

Une tuile de la tilemap est aussi identifiée par ses coordonnées (x_tuile, y_tuile), indiquées dans la zone d'info de l'éditeur, mais il faut bien comprendre que **ces coordonnées sont des multiples de 8 pixels pour un affichage dans la zone graphique du jeu**. On retrouve évidemment la même logique de coordonnées avec une origine au coin supérieur gauche et des abscisses croissantes vers la droite et des ordonnées croissantes vers le bas.

La zone de dessin de l'éditeur présente une portion de la tilemap de 16 x 16 tuiles, mais il est possible de naviguer ailleurs dans la tilemap. La taille d'une tilemap complète est de 256 x 256 tuiles, soit une zone graphique réelle de 2048 x 2048 pixels (ça laisse de quoi dessiner un beau décor !)

Notons enfin  qu'il existe 8 banques disponibles, indicées de 0 à 7. Dans un programme, la liste des tilemaps est accessible avec la variable `px.tilemaps`, et la tilemap n°i est donc accessible par `px.tilemaps[i]`.

## Appliquer des images et des tilemaps dans un jeu

Les deux fonctions permettant de dessiner des images dans la fenêtre du jeu, issues directement d'une banque d'images ou d'une tilemap, sont `px.blt` et `px.bltm`. Voyons leur paramètres en détail :

- `px.blt(x_ecran, y_ecran, banque n°, x_banq, y_banq, l_img, h_img)`

    - `x_ecran, y_ecran` : coordonnées du coin supérieur gauche de l'écran où l'on veut appliquer l'image
    - `banque n°` : indice de la banque d'images (entre 0 et 2)
    - `x_banq, y_banq` : coordonnées au sein de la banque du coin supérieur gauche de la portion que l'on veut sélectionner
    - `l_img, h_img` : largeur et hauteur de la portion que l'on veut sélectionner


- `px.bltm(x_ecran, y_ecran, tilemap n°, x_tm, y_tm, l_img, h_img)`

    - `x_ecran, y_ecran` : coordonnées du coin supérieur gauche de l'écran où l'on veut appliquer la tilemap
    - `tilemap n°` : indice de la tilemap (entre 0 et 7)
    - `x_tm, y_tm` : coordonnées au sein de la tilemap du coin supérieur gauche de la portion que l'on veut sélectionner. Attention, les coordonnées sont bien mesurées en pixels et non pas en "coordonnées de tuiles"
    - `l_img, h_img` : largeur et hauteur de la portion que l'on veut sélectionner (en pixels)

### Compléments :
On peut ajouter un paramètre optionnel à ces deux fonctions (`colkey`), qui est un nombre désignant une couleur de la palette (de 0 à 15) qui sera une couleur de transparence ; c'est à dire que tous les pixels de cette couleur dans l'image qu'on veut appliquer sur le jeu ne seront pas dessinés.

C'est utile par exemple pour que le fond de l'image d'un sprite (supposons noir) ne recouvre pas les pixels du décor dans lequel évolue ce sprite (supposons un ciel bleu).

Enfin, il est possible d'indiquer une valeur négative pour la largeur ou la hauteur d'une image à appliquer ;  dans ce cas, l'image sera retournée horizontalement ou verticalement.

C'est utile par exemple pour qu'une seule image de sprite permette de dessiner le sprite se déplaçant vers la gauche ou vers la droite (on utilise dans ce cas un retournement horizontal).
