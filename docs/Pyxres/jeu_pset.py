import pyxel as px

CARRE = (4, 2)
SPACE_SOUND = 2

TILEMAP = 0
X_TM, Y_TM = 0, 0
L_TM, H_TM = 128, 64
x_tm, y_tm = 0, 0

x, y = 0, 0

fps = 8
px.init(128, 64, title="test pset", fps=fps)

px.load("res.pyxres") 

temps = 0

def update():
    global temps, x, y
    temps = px.frame_count // fps 
    x = (x+1) % 32
    y = ((temps // 4) * 8) % 40
    
def draw():
    px.cls(0)
    px.bltm(x_tm, y_tm, TILEMAP, X_TM, Y_TM, L_TM, H_TM) 
    px.pset(x, y, 8) 
    
    coord_ecran = f"ecran : {int(x)}, {int(y)}"
    px.text(8, px.height - 8, coord_ecran, 8)
    
    tm_x, tm_y = int(x // 8), int(y // 8)
    coord_tilemap = f"tilemap : {tm_x}, {tm_y}"
    px.text(8, px.height - 16, coord_tilemap, 15)
    
    tuile_x, tuile_y = px.tilemaps[0].pget(tm_x, tm_y)
    tuile = f"tuile : {tuile_x}, {tuile_y}"
    px.text(72, px.height - 16, tuile, 7)
     
    if px.btn(px.KEY_SPACE):      
        px.play(0, SPACE_SOUND)
        px.tilemaps[0].pset(tm_x, tm_y, CARRE)
    
px.run(update, draw)
