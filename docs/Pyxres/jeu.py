import pyxel as px

TILEMAP = 0
X_TM, Y_TM = 0, 0
L_TM, H_TM = 64, 128
x_tm, y_tm = 32, 64

BANQ_IMG = 0
X_IMG, Y_IMG = 32, 0
L_IMG, H_IMG = 8, 8
x_img, y_img = 0, 0

fps = 30 
px.init(128, 128, title="test map", fps=fps)

px.load("res.pyxres") 

temps = 0

def update():
    global temps, x_img
    temps = px.frame_count // fps
    x_img = temps * 8
    
def draw():
    px.cls(0)
    px.bltm(x_tm, y_tm, TILEMAP, X_TM, Y_TM, L_TM, H_TM) 
    px.blt(x_img, y_img, BANQ_IMG, X_IMG, Y_IMG, L_IMG, H_IMG) 
    
px.run(update, draw)
