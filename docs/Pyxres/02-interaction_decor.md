---
author: profjahier
title: Interagir avec le décor
---

# Interagir avec le décor

## Identifier une tuile : méthode `pget`

Pour pouvoir programmer des interactions avec le décor (ou autre), il est très utile de savoir quelle tuile se trouve aux coordonnées (x, y) du jeu.

Par exemple, on désire savoir quel type de tuile (sol, mur, ciel, etc) se trouve aux coordonnées (x, y) de l'écran. 

### Lien entre les coordonnées en pixels (x, y) et les coordonnées "en tuiles" de la tilemap (x_tuile, y_tuile)
Dans une tilemap, tous les pixels de x = 0 à x = 7 appartiennent à la tuile avec x_tuile = 0, de même les pixels de x = 8 à x = 15 appartiennent à la tuile avec x_tuile = 1, et ainsi de suite. La division entière de x et y par 8 donne donc les coordonnées de la tuile dans la tilemap :

`#!py x_tuile, y_tuile = x // 8, y // 8`

### Coordonnées de tuile dans la tilemap et dans la banque d'images associée
À chaque coordonnée (x_tuile, y_tuile) de la tilemap est dessinée une tuile de la banque d'images. Au sein de la banque d'images, cette tuile est identifiable par des coordonnées de tuile (x_tuile_banq, y_tuile_banq), multiples aussi de 8 pixels.

!!! example "Exemple : Coordonnées de tuile de la banque"
    Dans la banque ci-dessous, la première tuile où on a dessiné "0 0" est aux coordonnées (x_tuile_banq = 0, y_tuile_banq = 0), la tuile où est dessiné le sprite est aux coordonnées  (x_tuile_banq = 4, y_tuile_banq = 0), le carré vert est aux coordonnées  (x_tuile_banq = 4, y_tuile_banq = 2), etc.
    
    
    Pour faciliter un peu la lecture, dans toute la partie haute à gauche de la banque, on a réalisé des dessins de nombres donnant les coordonnées des tuiles correspondantes (de (0, 0) à (3, 3)).

    ![tuile banque](banq_img.png){width=500}

### La méthode `pget`
La méthode `pget` d'une tilemap (*objet de la classe Tilemap*) prend en paramètres les coordonnées d'une tuile dans la tilemap (x_tuile, y_tuile), identifie la tuile qui est dessinée, et renvoie les coordonnées de cette tuile dans la banque d'images (x_tuile_banq, y_tuile_banq). 

Dans le code, une tilemap est identifiée par la variable `px.tilemaps[i]` (i est le numéro de la tilemap). 

On obtient donc un couple `(x_tuile_banq, y_tuile_banq)` en appelant la méthode `pget` sur cet objet : `px.tilemaps[i].pget(x_tuile, y_tuile)`.

!!! example "Exemple suite"
    La banque de l'exemple précédent a permis de construire la tilemap suivante :

    ![tilemap](tilemap.png){width=500}

    La première tuile sur la première ligne de la tilemap est aux coordonnées (x_tuile = 0, y_tuile = 0). La tuile associée est aux coordonnées (x_tuile_banq = 0, y_tuile_banq = 0). La tuile suivante (dessin du sprite) est aux coordonnées (x_tuile = 1, y_tuile = 0) et la tuile associée est aux coordonnées (x_tuile_banq = 4, y_tuile_banq = 0).

    Encore un exemple : une tuile de sol est dessinée aux coordonnées (x_tuile = 2, y_tuile = 1) et désigne la tuile (x_tuile_banq = 4, y_tuile_banq = 1). 
    
    Ainsi : `#!py px.tilemaps[0].pget(2, 1)`  vaut `#!py (4, 1)`.

**Attention:** ne pas confondre cette méthode avec la fonction `px.pget(x, y)` qui donne la couleur d'un pixel de l'écran.

La vidéo suivante présente un pixel rouge se baladant dans la zone de jeu. Les coordonnées du point de l'écran (x, y), de la tuile dans la tilemap (x_tuile, y_tuile) et dans la banque d'images (x_tuile_banq, y_tuile_banq) sont affichées dans le jeu. 
<video width="600"  controls autoplay loop>
  <source src="../pget.webm" type="video/webm" >
</video>

### Lien avec les coordonnées à l'écran

Considérons le cas simple, mais fréquent, où une portion de tilemap est appliquée aux coordonnées (0, 0) de l'écran de jeu, et que l'origine de la portion de la tilemap qu'on applique à l'écran soit aussi aux coordonnées (0, 0) dans la tilemap (mesurée en pixels).

Concrètement, on a écrit l'instruction `#!py px.bltm(0, 0, tilemap_i, 0, 0, l_tm, h_tm)`

Dans cette situation, les coordonnées en pixels (x, y) à l'écran sont tout simplement les mêmes que dans la tilemap.

Ainsi pour savoir quel type de tuile se trouve aux coordonnées (x, y) de l'écran, on appelle directement la méthode `px.tilemaps[i].pget(x, y)` pour obtenir ses coordonnées dans la banque d'images ! Évidemment, il faut savoir quel type de tuile on a dessiné dans la banque d'images...

!!! example "Patron de programme illustrant ce principe"
    ```python
    # coordonnées (x_tuile_banq, y_tuile_banq) de certaines tuiles du décor :
    MUR = (4, 2)   
    SOL = (5, 1)
    ...
    x_joueur, y_joueur = 64, 120
    ...
    tuile = px.tilemaps[0].pget(x_joueur, y_joueur)
    if tuile == MUR:
        # code pour une collision dans le mur
    elif tuile == SOL:
        # code pour un contact avec le sol
    # etc ...
    ```

**Exemple d'application :** Cela peut être utile par exemple si on veut qu'une action particulière soit entreprise en fonction de la position du joueur.

## Modifier la tilemap : méthode `pset`

La méthode `pset` d'une tilemap permet de définir une tuile dans une tilemap. Cette méthode attend 3 paramètres :

- x_tuile, y_tuile : coordonnées (en tuiles) dans la tilemap où dessiner une nouvelle tuile
- tuile : un tuple (x_tuile_banq, y_tuile_banq) identifiant une tuile de la banque d'images

**Exemple d'application :** Cela peut être utile par exemple si on veut que le décor soit modifié en fonction des actions du joueur.


!!! example "exemple : le joueur peut déposer des pièges là où il passe en appuyant sur <kbd>Espace</kbd>"
    ```python
    # coordonnées (x_tuile_banq, y_tuile_banq) d'une tuile "piège" :
    PIEGE = (4, 2)    
    ... 
    if px.btn(px.KEY_SPACE):
        x_tuile, y_tuile = x_joueur // 8, y_joueur // 8
        px.tilemaps[0].pset(x_tuile, y_tuile, PIEGE) 
    ```

La vidéo suivante reprend le même exemple que précédemment, mais lorsque l'utilisateur appuie sur la barre espace, une tuile de type "carré vert" est appliquée aux coordonnées du pixel qui parcourt la zone de jeu. La tilemap se trouve ainsi modifiée au cours du temps. 
<video width="600"  controls autoplay>
  <source src="../pset.webm" type="video/webm" >
</video>
